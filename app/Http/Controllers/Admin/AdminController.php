<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\Models\Channel;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('backend.dashboard');
    }

    public function logout()
    {
        Auth::guard()->logout();
        return redirect('/');
    }

    public function distance(){

        $data     = array();
        foreach(Channel::all() as $channel){
            $lat = 6.841587677300529; //latitude
            $lon = 80.96306940215365; //longitude
            
            $R = 6371; 

            $latFrom = deg2rad($lat);
            $lonFrom = deg2rad($lon);
            $latTo = deg2rad($channel->latitude);
            $lonTo = deg2rad($channel->longitude);

            $latDelta = $latTo - $latFrom;
            $lonDelta = $lonTo - $lonFrom;

            $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) + cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
            $distance = $angle * $R;
            echo $channel->name.'-'.$distance.'<br>';

            
            array_push(
                $data,
                array(
                    "id"          => $channel->id,
                    "name"        => $channel->name,
                    "latitude"    => $channel->latitude,
                    "longitude"   => $channel->longitude,
                    "description" => $channel->description,
                    "radius"      => $channel->radius,
                    "distance"    => $distance,
                    "banners"     => $channel->channel_banners() ? $channel->channel_banners()->pluck('banner') : [],
                )
            );

            
        }
        usort($data, function($a, $b) {
            return $a['distance'] <=> $b['distance'];
        });
        dd($data);
    }
}
