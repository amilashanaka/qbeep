<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;

use App\Models\Channel;

class ChannelController extends Controller
{
 	public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $table_data = Channel::all();
        return view('backend.channels', compact('table_data'));
    }

    function create () {
        $form_data= '';
        return view('backend.channels_add', compact('form_data'));
    }
    
    public function edit($id)
    {
        $form_data = Channel::find($id);
        return view('backend.channels_add', compact('form_data'));
    }

    public function store(Request $request){
        $validate= $request->validate([
            'name' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'description' => 'required',
            'temp_image'    => 'required',
            'temp_image.0'    => 'required',
        ], [
            'temp_image.0.required' => 'Please select atleast one image'
        ]);

        try {
	        $channel = Channel::create([
	        	'name' => $request->name,
	            'latitude' => $request->latitude,
	            'longitude' => $request->longitude,
	            'description' => $request->description,
	            'radius' => $request->radius,
	        ]);

            foreach ($request->temp_image as $image){
                if($image){
                    $channel->channel_banners()->create([
                        'banner' => $image
                    ]);
                }

            }

	        return redirect()->back()->with('success', 'Record Added...');

        } catch (Exception $e) {
        	
            return redirect()->back()->with('error', 'Data Inserting Error ..!');
        }
    }

    public function update(Request $request, $id){
        $validate= $request->validate([
            'name' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'description' => 'required',
        ]);

        try {
            $channel = Channel::find($id);
	        $channel->update([
	        	'name' => $request->name,
	            'latitude' => $request->latitude,
	            'longitude' => $request->longitude,
	            'description' => $request->description,
	            'radius' => $request->radius,
	        ]);

            $channel->channel_banners()->delete();
            foreach ($request->temp_image as $image){

                if($image){
                    $channel->channel_banners()->create([
                        'banner' => $image
                    ]);
                }
            }

	        return redirect()->back()->with('success', 'Record updated...');

        } catch (Exception $e) {

            return redirect()->back()->with('error', 'Data updating Error ..!');
        }
    }

    public function addImages(Request $request){

    // Temporarily increase memory limit to 256MB
        ini_set('memory_limit','256M');

        $image = $request->file('media');
        
        $filename = time()."-".$image->getClientOriginalName();
        $destinationPath = public_path('/images/banners');
        $img = Image::make($image->move($destinationPath, $filename))->resize(800, 400, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
        });
        $img->save();
        $temp_image_1= asset('images/banners/'.$filename);

        echo $temp_image_1;

    }
}
