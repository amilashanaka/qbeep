<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use Carbon\Carbon;

use App\Models\FeedTopic;

class FeedController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $table_data = FeedTopic::all();
        return view('backend.feeds', compact('table_data'));
    }

    function view ($id) {
        $feed = FeedTopic::find($id);
        return view('backend.feeds_view', compact('feed'));
    }
}
