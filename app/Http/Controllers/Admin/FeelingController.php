<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;

use App\Models\Feeling;

class FeelingController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $table_data = Feeling::all();
        return view('backend.feelings', compact('table_data'));
    }

    function create () {
        $form_data= '';
        return view('backend.feelings_add', compact('form_data'));
    }

    public function edit($id)
    {
        $form_data = Feeling::find($id);
        return view('backend.feelings_add', compact('form_data'));
    }

    public function store(Request $request){

        ini_set('memory_limit','256M');
        $validate= $request->validate([
            'description' => 'required',
            'image'    => 'required',
        ]);

        try {

            $image = $request->file('image');
            
            $filename = time()."-".$image->getClientOriginalName();
            $destinationPath = public_path('/images/feelings');
            $img = Image::make($image->move($destinationPath, $filename))->resize(300, 300, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
            });
            $img->save();
            $temp_image= asset('images/feelings/'.$filename);

            Feeling::create([
                'description' => $request->description,
                'image' => $temp_image,
            ]);

            return redirect()->back()->with('success', 'Record Added...');

        } catch (Exception $e) {
            
            return redirect()->back()->with('error', 'Data Inserting Error ..!');
        }
    }

    public function update(Request $request, $id){

        ini_set('memory_limit','256M');

        $validate= $request->validate([
            'description' => 'required',
        ]);

        try {

            $image = $request->file('image');
            $temp_image= $request->img_temp;

            if(!empty($image)){
                $filename = time()."-".$image->getClientOriginalName();
                $destinationPath = public_path('/images/feelings');
                $img = Image::make($image->move($destinationPath, $filename))->resize(300, 300, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                });
                $img->save();
                $temp_image= asset('images/feelings/'.$filename);
            }

            Feeling::find($id)->update([
                'description' => $request->description,
                'image' => $temp_image,
            ]);

            return redirect()->back()->with('success', 'Record Added...');

        } catch (Exception $e) {
            
            return redirect()->back()->with('error', 'Data Inserting Error ..!');
        }


    }
}
