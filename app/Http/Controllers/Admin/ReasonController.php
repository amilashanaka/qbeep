<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;

use App\Models\Reason;

class ReasonController extends Controller
{
    
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $form_data= '';
        $table_data = Reason::all();
        return view('backend.reasons', compact('table_data','form_data'));
    }

    public function edit($id)
    {
        $form_data = Reason::find($id);
        $table_data = Reason::all();
        return view('backend.reasons', compact('table_data','form_data'));
    }

    public function store(Request $request){

        
        $validate= $request->validate([
            'description' => 'required',
        ]);

        try {

            Reason::create([
                'description' => $request->description,
            ]);

            return redirect()->back()->with('success', 'Record Added...');

        } catch (Exception $e) {
            
            return redirect()->back()->with('error', 'Data Inserting Error ..!');
        }
    }

    public function update(Request $request, $id){

        $validate= $request->validate([
            'description' => 'required',
        ]);

        try {

            Reason::find($id)->update([
                'description' => $request->description
            ]);

            return redirect()->back()->with('success', 'Record Added...');

        } catch (Exception $e) {
            
            return redirect()->back()->with('error', 'Data Inserting Error ..!');
        }


    }
}
