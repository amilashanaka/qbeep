<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Models\User;

class UserController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    function influencers(){
        
        $table_data = User::where('user_type_id', 2)->get();
        return view('backend.influencers', compact('table_data'));
    }

    function appUsers(){

        $table_data = User::where('user_type_id', 3)->get();
        return view('backend.app_users', compact('table_data'));
        
    }
    
    public function index()
    {
        
        $table_data = User::where('user_type_id', 1)->get();
        $form_data = "";
        return view('backend.users', compact('table_data','form_data'));
    }

    function store(Request $request) {

        $validate= Validator::make($request->all(), [
            'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if($validate->fails()){
            return back()->withErrors($validate)->withInput($request->all());
        }

        $user = new User;

        $user->firstname  = $request->firstname;
        $user->lastname  = $request->lastname;
        $user->email  = $request->email;
        $user->password  =  Hash::make($request->password);
        $user->status  = '1';
        $user->is_admin = '1';

        try {

            $user->save();

            return redirect()->back()->with('success', 'Data Successfuly Saved ..!');

        } catch (Exception $e) {

            return redirect()->back()->with('error', 'Data Inserting Error ..!');
        }

    }

    function edit($id){
        
        $table_data = User::where('is_admin', '1')->get();
        $form_data = User::find($id);
        return view('backend.users_edit', compact('table_data', 'form_data'));
    }

    function update(Request $request, $id){

        $validate= $request->validate([
            'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($id)],
        ]);

        $user = User::find($id);

        $user->firstname  = $request->firstname;
        $user->lastname  = $request->lastname;
        $user->email  = $request->email;

        try {

            $user->save();
            return redirect()->back()->with('success', 'Data successfuly Updated ..!');

        } catch (Exception $e) {

            return redirect()->back()->with('error', 'Data Updating Error ..!');
        }
    }

    function updatePassword(Request $request, $id){

        $validate= $request->validate([
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);


        $user = User::find($id);

        $user->password  =  Hash::make($request->password);

        try {

            $user->save();
            return redirect()->back()->with('success', 'Data successfuly Updated ..!');

        } catch (Exception $e) {

            return redirect()->back()->with('error', 'Data Updating Error ..!');
        }

    }
}
