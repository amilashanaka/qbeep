<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;

use App\Models\UserType;

class UserTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $table_data = UserType::all();
        $form_data = "";
        return view('backend.user_types', compact('table_data','form_data'));
    }
    
    public function edit($id)
    {
        $table_data = UserType::all();
        $form_data = UserType::find($id);
        return view('backend.user_types', compact('table_data','form_data'));
    }

    public function store(Request $request){
        $validate= $request->validate([
            'name' => ['required', 'string', 'max:255', 'unique:user_types'],
        ]);

        UserType::create([
            'name' => $request->name
        ]);

        return redirect()->back()->with('success', 'Record Added...');
    }

    public function update(Request $request, $id){
        $validate= $request->validate([
            'name' => ['required', 'string', 'max:255', Rule::unique('user_types')->ignore($id)],
        ]);

        UserType::find($id)->update([
            'name' => $request->name
        ]);

        return redirect()->back()->with('success', 'Record updated...');
    }
}
