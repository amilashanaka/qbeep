<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Api\ResponseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;


use App\Models\User;
use App\Models\Channel;
use App\Models\FeedTopic;
use App\Models\Feeling;
use App\Models\FeedTopicLike;
use App\Models\Comment;
use App\Models\Reason;

class ApiController extends ResponseController
{

    // public function getChannels()
    // {
    //     $lat      = request('lat');
    //     $lon      = request('lon');
    //     $distance = 5; //your distance in KM
    //     $R        = 6371; //constant earth radius. You can add precision here if you wish

    // $maxLat = $lat + rad2deg($distance / $R);
    // $minLat = $lat - rad2deg($distance / $R);
    // $maxLon = $lon + rad2deg(asin($distance / $R) / cos(deg2rad($lat)));
    // $minLon = $lon - rad2deg(asin($distance / $R) / cos(deg2rad($lat)));

    //     $channels = Channel::all();
    //     if(request()->user()->user_type_id == '2'){

    //         $channels = Channel::whereBetween('longitude', [$minLon, $maxLon])->whereBetween('latitude', [$minLat, $maxLat])->get();
    //     }

    //     $data     = array();
    //     foreach ($channels as $key => $channel) {
    //         array_push(
    //             $data,
    //             array(
    //                 "id"          => $channel->id,
    //                 "name"        => $channel->name,
    //                 "latitude"    => $channel->latitude,
    //                 "longitude"   => $channel->longitude,
    //                 "description" => $channel->description,
    //                 "radius"      => $channel->radius,
    //                 "banners"     => $channel->channel_banners() ? $channel->channel_banners()->pluck('banner') : [],
    //             )
    //         );
    //     }

    //     return $this->sendResponse($data);
    // }

    public function getChannels()
    {
        $lat = request('lat');
        $lon = request('lon');
        $R   = 6371; //constant earth radius. You can add precision here if you wish

        $channels = Channel::all();
        $data     = array();

        foreach ($channels as $key => $channel) {
            // calculate distance
            $latFrom = deg2rad($lat);
            $lonFrom = deg2rad($lon);
            $latTo   = deg2rad($channel->latitude);
            $lonTo   = deg2rad($channel->longitude);

            $latDelta = $latTo - $latFrom;
            $lonDelta = $lonTo - $lonFrom;

            $angle    = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) + cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
            $distance = $angle * $R;

            // calculate range
            $is_in_range = false;
            $maxLat      = $lat + rad2deg($channel->radius / $R);
            $minLat      = $lat - rad2deg($channel->radius / $R);
            $maxLon      = $lon + rad2deg(asin($channel->radius / $R) / cos(deg2rad($lat)));
            $minLon      = $lon - rad2deg(asin($channel->radius / $R) / cos(deg2rad($lat)));

            if (($maxLat > $channel->latitude && $minLat < $channel->latitude) && ($maxLon > $channel->longitude && $minLon < $channel->longitude)) {
                $is_in_range = true;
            }

            array_push(
                $data,
                array(
                    "id"          => $channel->id,
                    "name"        => $channel->name,
                    "latitude"    => $channel->latitude,
                    "longitude"   => $channel->longitude,
                    "description" => $channel->description,
                    "radius"      => $channel->radius,
                    "distance"    => $distance,
                    "is_in_range" => $is_in_range,
                    "banners"     => $channel->channel_banners() ? $channel->channel_banners()->pluck('banner') : [],
                )
            );
        }
        // sort array distance wise
        usort(
            $data,
            function ($a, $b) {
                return $a['distance'] <=> $b['distance'];
            }
        );

        return $this->sendResponse($data);
    }

    public function getFeeds()
    {
        if (!request('channel_id')) {
            $feeds = [];
        }

        $feeds = FeedTopic::where('channel_id', request('channel_id'))->get();
        $data  = array();
        foreach ($feeds as $key => $feed) {
            $user_liked    = $feed->feed_topic_likes()->where('user_id', request()->user()->id)->where('type', 1)->first();
            $like_count    = $feed->feed_topic_likes()->where('type', 1)->count();
            $user_disliked = $feed->feed_topic_likes()->where('user_id', request()->user()->id)->where('type', 2)->first();
            $dislike_count = $feed->feed_topic_likes()->where('type', 2)->count();

            array_push(
                $data,
                array(
                    'id'            => $feed->id,
                    'description'   => $feed->description,
                    'media'         => $feed->feed_topic_medias,
                    'created_at'    => $feed->created_at,
                    'liked'         => $user_liked ? 'true' : 'false',
                    'like_count'    => $like_count,
                    'disliked'      => $user_disliked ? 'true' : 'false',
                    'dislike_count' => $dislike_count,
                    'user'          => User::find($feed->user_id),
                    'channel'       => Channel::find($feed->channel_id),
                    'feeling'       => $feed->feeling
                )
            );
        }

        return $this->sendResponse($data);
    }

    function storeFeeds(Request $request)
    {
        ini_set('max_execution_time', 800);
        ini_set("memory_limit", "800M");
        ini_set("post_max_size ", "1024M");
        ini_set("upload_max_filesize ", "1024M");

        $validator = Validator::make(
            $request->all(),
            [
                'channel_id'  => 'required',
                'description' => 'required',
            ]
        );

        if ($validator->fails()) {
            return $this->sendError($validator->errors());
        }

        try {
            $feed = FeedTopic::create(
                [
                    'description' => $request->description,
                    'user_id'     => $request->user()->id,
                    'channel_id'  => $request->channel_id,
                    'feeling_id'  => $request->feeling_id,

                ]
            );

            if ($request->hasfile('media')) {
                $files = $request->file('media');
                foreach ($files as $file):
                    $mime = $file->getClientMimeType();
                    if (strstr($mime, "video/")) {
                        $filename        = time()."-".$request->user()->id."-".$file->getClientOriginalExtension();
                        $destinationPath = public_path('/images/feed/videos');
                        $file->move($destinationPath, $filename);

                        $feed->feed_topic_medias()->create(
                            [
                                'media_url' => asset('images/feed/videos/'.$filename),
                                'type'      => 'video'
                            ]
                        );
                    } else {
                        if (strstr($mime, "image/")) {
                            $filename        = time()."-".$request->user()->id."-".$file->getClientOriginalExtension();
                            $destinationPath = public_path('/images/feed/images');
                            $file->move($destinationPath, $filename);

                            $feed->feed_topic_medias()->create(
                                [
                                    'media_url' => asset('images/feed/images/'.$filename),
                                    'type'      => 'image'
                                ]
                            );
                        }
                    }

                endforeach;
            }

            if ($request->hasfile('single_media')) {
                $file = $request->file('single_media');
                $mime = $file->getClientMimeType();
                if (strstr($mime, "video/")) {
                    $filename        = time()."-".$request->user()->id."-".$file->getClientOriginalExtension();
                    $destinationPath = public_path('/images/feed/videos');
                    $file->move($destinationPath, $filename);

                    $feed->feed_topic_medias()->create(
                        [
                            'media_url' => asset('images/feed/videos/'.$filename),
                            'type'      => 'video'
                        ]
                    );
                } else {
                    if (strstr($mime, "image/")) {
                        $filename        = time()."-".$request->user()->id."-".$file->getClientOriginalExtension();
                        $destinationPath = public_path('/images/feed/images');
                        $file->move($destinationPath, $filename);

                        $feed->feed_topic_medias()->create(
                            [
                                'media_url' => asset('images/feed/images/'.$filename),
                                'type'      => 'image'
                            ]
                        );
                    }
                }
            }


            $success = "Upload Success.";

            return $this->sendResponse($success);
        } catch (Exception $e) {
            $error = "Error in Uploading.";

            return $this->sendError($error, 401);
        }
    }

    function getReasons()
    {
        $data = Reason::all();

        return $this->sendResponse($data);
    }

    function getFeelings()
    {
        $data = Feeling::all();

        return $this->sendResponse($data);
    }

    function storeLikes(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'feed_id' => 'required',
                'status'  => 'required',
                'type'    => 'required'
            ]
        );

        if ($validator->fails()) {
            return $this->sendError($validator->errors());
        }

        try {
            $is_exist = FeedTopicLike::where('feed_topic_id', $request->feed_id)->where('user_id', $request->user()->id)->first();

            if ($is_exist) {
                $is_exist->delete();
            } else {
                $feed = FeedTopicLike::create(
                    [
                        'user_id'       => $request->user()->id,
                        'feed_topic_id' => $request->feed_id,
                        'type'          => $request->type == 'like' ? '1' : '2',

                    ]
                );
            }


            $success = "Success.";

            return $this->sendResponse($success);
        } catch (Exception $e) {
            $error = "Error.";

            return $this->sendError($error, 401);
        }
    }

    function getComments()
    {
        $comments = Comment::where('feed_topic_id', request('feed_id'))->whereNull('comment_id')->get();
        $data     = array();

        foreach ($comments as $key => $comment) {
            $is_flagged = $comment->comment_flags()->where('user_id', request()->user()->id)->first();

            $child_comments = array();
            foreach ($comment->comments as $value) {
                array_push(
                    $child_comments,
                    array(
                        'id'         => $value->id,
                        'comment'    => $value->comment,
                        'media'      => $value->media_url,
                        'media_type' => $value->media_type,
                        'is_hide'    => $value->is_hide,
                        'feed_id'    => $value->feed_topic_id,
                        'created_at' => $value->created_at,
                        'user'       => User::find($value->user_id),
                    )
                );
            }

            array_push(
                $data,
                array(
                    'id'             => $comment->id,
                    'comment'        => $comment->comment,
                    'media'          => $comment->media_url,
                    'media_type'     => $comment->media_type,
                    'is_hide'        => $comment->is_hide,
                    'child_comments' => $child_comments,
                    'feed_id'        => $comment->feed_topic_id,
                    'created_at'     => $comment->created_at,
                    'user'           => User::find($comment->user_id),
                    'is_flagged'     => $is_flagged ? true : false
                )
            );
        }

        return $this->sendResponse($data);
    }

    function storeComments(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'feed_id' => 'required',
                'comment' => 'required',
            ]
        );

        if ($validator->fails()) {
            return $this->sendError($validator->errors());
        }

        try {
            Comment::create(
                [
                    'comment'       => $request->comment,
                    'comment_id'    => $request->comment_id,
                    'feed_topic_id' => $request->feed_id,
                    'user_id'       => $request->user()->id,
                ]
            );


            $comments = Comment::where('feed_topic_id', request('feed_id'))->get();
            $data     = array();
            foreach ($comments as $key => $comment) {
                array_push(
                    $data,
                    array(
                        'id'             => $comment->id,
                        'comment'        => $comment->comment,
                        'media'          => $comment->media_url,
                        'media_type'     => $comment->media_type,
                        'is_hide'        => $comment->is_hide,
                        'child_comments' => $comment->comments,
                        'feed_id'        => $comment->feed_topic_id,
                        'created_at'     => $comment->created_at,
                        'user'           => User::find($comment->user_id),
                    )
                );
            }

            return $this->sendResponse($data);
        } catch (Exception $e) {
            $error = "Error.";

            return $this->sendError($error, 401);
        }
    }

    function flagComments(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'feed_id'     => 'required',
                'comment_id'  => 'required',
                'description' => 'required',
            ]
        );

        if ($validator->fails()) {
            return $this->sendError($validator->errors());
        }

        try {
            $request->user()->comment_flags()->create(
                [
                    'description'   => $request->description,
                    'comment_id'    => $request->comment_id,
                    'feed_topic_id' => $request->feed_id,

                ]
            );

            $success = "Success";

            return $this->sendResponse($success);
        } catch (Exception $e) {
            $error = "Error";

            return $this->sendError($error, 401);
        }
    }


    public function check_text(Request $request)
    {
        $out = array();


        $validator = Validator::make(
            $request->all(),
            [
                'body'       => 'required',
                'comment_id' => 'required',
                'post_id'    => 'required',
            ]
        );


        if ($validator->fails()) {
            return $this->sendError($validator->errors());
        }


        $body   = $request->body;




        if (!preg_match("/[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+.[a-zA-Z]{2,4}/", $body)){
            array_push($out, "email", false);
        }else{

            array_push($out, "email", true);
        }

        if (preg_match("/www/", $body)){
            array_push($out, "url", true);
        }else{

            array_push($out, "url", false);
        }



        if (preg_match("/[0-9]{10}/s", $body)){
            array_push($out, "phone", true);
        }else{


            array_push($out, "phone", false);
        }

        return $this->sendResponse($out);
    }






    // function distance(){
    //     $lat = 6.841587677300529; //latitude
    //     $lon = 79.96306940215365; //longitude
    //     $to_lat= 6.847264835489733;
    //     $to_lon= 79.95199144887664;
    //     $distance = 5; //your distance in KM
    //     $R = 6371; //constant earth radius. You can add precision here if you wish

    //     $maxLat = $lat + rad2deg($distance/$R);
    //     $minLat = $lat - rad2deg($distance/$R);
    //     $maxLon = $lon + rad2deg(asin($distance/$R) / cos(deg2rad($lat)));
    //     $minLon = $lon - rad2deg(asin($distance/$R) / cos(deg2rad($lat)));

    //     if(($maxLat > $to_lat && $minLat < $to_lat) && ($maxLon > $to_lon && $minLon < $to_lon)){

    //         $latFrom = deg2rad($lat);
    //         $lonFrom = deg2rad($lon);
    //         $latTo = deg2rad($to_lat);
    //         $lonTo = deg2rad($to_lon);

    //         $latDelta = $latTo - $latFrom;
    //         $lonDelta = $lonTo - $lonFrom;

    //         $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) + cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
    //         $distance = $angle * $R;
    //         echo $distance.'<br>';

    //     }

    //     echo $maxLat, "<br>", $minLat, "<br>", $maxLon, "<br>", $minLon;

    // }
}


