<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Api\ResponseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;


use App\Models\User;

class AuthController extends ResponseController
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile_no' => 'required',
            'device_id' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'user_type' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError($validator->errors());       
        }

        $image = $request->file('profile_img');
        $profile_image = '';
        if(!empty($image)){
            $filename = time()."-".$request->user()->id."-".$image->getClientOriginalExtension();
            $destinationPath = public_path('/images/profile');
            $image->move($destinationPath, $filename);

            $profile_image= asset('images/profile/'.$filename);
        }

        $user = User::where('mobile_no', $request->mobile_no)->first();

        if(!$user){
            $user= User::create([
                'firstname' => $request->firstname,
                'lastname' => $request->lastname,
                'device_id' => $request->device_id,
                'mobile_no' => $request->mobile_no,
                'user_type_id' => $request->user_type,
                'profile_img' => $profile_image,
            ]);
        }else {
            if(!empty($image)){
                $user->update([
                   'profile_img' => $profile_image 
                ]);
            }  
        }

        

        // if (!Hash::check($request->password, $user->password)) {
        //     $error = "Unauthorized";
        //     return $this->sendError($error, 401);
        // }

        Auth::login($user);
        
        if(Auth::user()->is_admin){
            $error = "Unauthorized";
            return $this->sendError($error, 401);
        }


        $success['token'] =  Auth::user()->createToken('token')->accessToken;
        $success['user'] =  Auth::user();
        return $this->sendResponse($success);



    }

    function getUsers(){
        $users = User::where('user_type_id','!=','1')->get();
        return $this->sendResponse($users);
        
    }
}
