<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ResponseController extends Controller
{
    
    public function sendResponse($data)
    {
        $response = [
            'success' => true,
            'data' => $data,
        ];
        return response()->json($response, 200);
    }


    public function sendError($error, $code = 404)
    {
        $response = [
            'success' => false,
            'error' => $error,
        ];
        return response()->json($response, $code);
    }
}
