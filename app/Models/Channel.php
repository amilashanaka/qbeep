<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function channel_banners(){
        return $this->hasMany(ChannelBanner::class);
    }

    public function channel_medias(){
        return $this->hasMany(ChannelMedia::class);
    }

    public function feed_topics(){
        return $this->hasMany(FeedTopic::class);
    }
}
