<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function feed_topic(){
        return $this->belongsTo(FeedTopic::class);
    }

    public function comment(){
        return $this->belongsTo(Comment::class);
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function comment_flags(){
        return $this->hasMany(CommentFlag::class);
    }
}
