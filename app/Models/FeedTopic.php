<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FeedTopic extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function channel(){
        return $this->belongsTo(Channel::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function feeling(){
        return $this->belongsTo(Feeling::class);
    }

    public function feed_topic_medias(){
        return $this->hasMany(FeedTopicMedia::class);
    }

    public function feed_topic_likes(){
        return $this->hasMany(FeedTopicLike::class);
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function comment_flags(){
        return $this->hasMany(CommentFlag::class);
    }
}
