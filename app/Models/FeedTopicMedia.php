<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FeedTopicMedia extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function feed_topic(){
        return $this->belongsTo(FeedTopic::class);
    }
}
