<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

        $types = array("Admin", "Influencer", "User"); 

        foreach($types as $type){
                DB::table('user_types')->insert([
                        'name' => $type,
                        'status' => '1',
                ]);
        }
    }
}
