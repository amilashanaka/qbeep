var myBtnID = "";
var myFrameID = "";

function newwin(id,title,winsrc){
	if($(id).length){
		$(".frameclass").hide();
		$(id).show();
		myBtnID = "#btn_" + id;
		myFrameID = "#" + id;
	}else{
		$(myBtnID).after('<div id="btn_' + id + '" class="navbtn" ><div class="contentbox" onclick="closewin(\'#btn_' + id + '\',\'#myFrame_' + id + '\')"><img src="images/button/menubtn_cancel.png" /></div><div id="title_' + id + '" class="textbox subbox" onclick="openwin(\'' + id + '\',\'' + title + '\',\'' + winsrc + '\')"><br />' + title + '</p></div></div>');
		openwin(id,title,winsrc,true);
	}
}

function openwin(id,title,winsrc,issub){
	issub = typeof issub !== 'undefined' ? issub : false;
	if($(".navbtn.normal").length >= 10){
		alert("Maximum number of opened tab reached. Close some tabs if want to open new tab.");
	}else{
		if($("#myFrame_" + id).length){
			$(".frameclass").hide();
			$("#myFrame_" + id).show();
			myBtnID = "#TabBtn_" + id;
			myFrameID = "#myFrame_" + id;
			$('.navbtn.normal').each(function(index, obj){
				$(obj).css("background-color","#848484");
			});
			$(myBtnID).css("background-color","#3F3F3F");
		}else{
			$('<iframe />', {
				name: 'myFrame_' + id,
				id:   'myFrame_' + id,
				class: 'frameclass',
				src: winsrc
			}).appendTo('#rightmenu');
			if(!issub){
				$('.navbtn.normal').each(function(index, obj){
					$(obj).css("background-color","#848484");
				});
				$("#menu_" + id).toggleClass("selected");
				$('<div />', {
					name: 'TabBtn_' + id,
					id:   'TabBtn_' + id,
					tag: id,
					class: 'navbtn normal',
					html: title,
					click: function(){
						//alert( id );
						openwin(id,"","#",false);
					}
				}).appendTo('#tabmenu');
			}
			myBtnID = "#TabBtn_" + id;
			myFrameID = "#myFrame_" + id;
		}
	}
}

function closewin(id_btn,id_frame){
	$(id_btn).remove();
	$(id_frame).remove();
}

function closemaster(id){
	$("#toolbar_" + id).hide();
	$("#myFrame_" + id).remove();
	$("#menu_" + id).toggleClass("selected");
}


