

@extends('backend.layouts.master')

@section('content')
   <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <h3>Channels Create</h3>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
                <div class="row">
                    <div class="col-12">
                        <!-- /.card -->

                       <div class="card-body">
           <div class="row">
              <div class="col-md-12">

              
              <form action="{{ !$form_data?url('admin/channels'):url('admin/channels/'.$form_data->id) }}" method="post" enctype="multipart/form-data" >
              
              @csrf

              @include('backend.layouts.errors')

                <div class="form-group col-md-6">
                  <label>Name</label>
                  <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $form_data->name ?? old('name')}}" required="" placeholder="Name">
                </div>

                
                <div class="form-group col-md-6">
                  <label>Latitude</label>
                  <input type="text" class="form-control @error('latitude') is-invalid @enderror" name="latitude" value="{{ $form_data->latitude ?? old('latitude')}}" required="" placeholder="Latitude">
                </div>

                <div class="form-group col-md-6">
                  <label>Longitude</label>
                  <input type="text" class="form-control @error('longitude') is-invalid @enderror" name="longitude" value="{{ $form_data->longitude ?? old('longitude')}}" required="" placeholder="Longitude">
                </div>



                <div class="form-group col-md-6">
                  <label>Radius (km)</label>
                  <input type="text" class="form-control @error('radius') is-invalid @enderror" name="radius" value="{{ $form_data->radius ?? old('radius')}}" required="" placeholder="Radius">
                </div>

                <div class="form-group col-md-6">
                  <label>Description</label>
                  <textarea name="description" rows="8" class="form-control @error('description') is-invalid @enderror">{{$form_data->description ?? old('description')}}</textarea>
                </div>


                

               <div class="form-group col-xl-6 col-md-12">
                  <label for="exampleInputEmail1">Banners (Add upto 10 images *)</label>
                  <div class="row">

                    @for($i=0;$i<10;$i++)
                    @php
                      if($i){
                        $media = $form_data && $form_data->channel_banners->get($i)?$form_data->channel_banners->get($i)->banner:'';
                        $banner = $form_data && $form_data->channel_banners->get($i)?$form_data->channel_banners->get($i)->banner:asset('images/no-preview.png');
                      } else {
                        $media = $form_data && $form_data->channel_banners->get($i)?$form_data->channel_banners->first()->banner:'';
                        $banner = $form_data && $form_data->channel_banners->get($i)?$form_data->channel_banners->first()->banner:asset('images/no-preview.png');
                      }
                    @endphp
                      <div class="col-md-2">
                      <div class="image-upload">
                        <label for="pro_img_{{$i+1}}">
                            <img 
                              id="pro_image_preview_{{$i+1}}" 
                              src="{{$banner}}" 
                              style="width: 100px; height: 80px;cursor: pointer;" 
                            />
                        </label>
                        <input id="pro_img_{{$i+1}}" type="file" style="display: none;" name="media[]" />
                        <input type='hidden' id="temp_img_{{$i+1}}" name='temp_image[]' value='{{$media}}'>
                      </div>
                      </div>
                    @endfor
                    </div>
                

                </div>

                <div class="form-group">
                  <button class="btn btn-lg btn-dark rounded px-5">Submit</button>
                </div>
                
                
              </form>
            </div>
          </div>
        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
    <!-- /.content -->
  </div>


@endsection
