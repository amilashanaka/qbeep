

@extends('backend.layouts.master')

@section('content')
   <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <h3>Feed List</h3>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
                <div class="row">
                    <div class="col-12">
                        <!-- /.card -->

                        <div class="card">
                          
                            <div class="card-header">
                                <h3 class="card-title" >
                                    
                                   
                                </h3>
                            </div>
                         
                            <!-- /.card-header -->
                            <div class="card-body">
                              <table id="example1" class="table table-bordered table-striped">
                                  <thead>
                                    <tr>
                                      <th>#</th>
                                      <th>User</th>
                                      <th>Channel</th>
                                      <th>Created at</th>
                                      <th>Action</th>
                                    </tr>
                                  </thead>
                                  <tbody class="brand-table-body">
                                    @foreach($table_data as $key => $value)
                                      <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $value->user->firstname.' '.$value->user->lastname }}</td>
                                        <td>{{ $value->channel->name }}</td>
                                        <td>{{ $value->created_at->toDateString() }}</td>
                                        <td><a class="btn btn-md btn-primary" href="{{ url('admin/feeds/'.$value->id.'/view') }}">View</a></td>
                                      </tr>
                                    @endforeach
                                  
                                  </tbody>
                              </table>
                            </div>
                            <!-- /.card-body -->
                          </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
    <!-- /.content -->
  </div>


@endsection
