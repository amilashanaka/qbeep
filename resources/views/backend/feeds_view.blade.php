

@extends('backend.layouts.master')

@section('content')
   <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <h3>Feed List</h3>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
                <div class="row">
                    <div class="col-12">
                        <!-- /.card -->

                        <div class="card">
                          
                            <div class="card-header">
                                <h3 class="card-title" >
                                    
                                   
                                </h3>
                            </div>
                         
                            <!-- /.card-header -->
                            <div class="card-body">
                              <div class="post">
                                <div class="user-block">
                                  <img class="img-circle img-bordered-sm" src="{{$feed->user->profile_img ?? 'http://placehold.it/300x300'}}" alt="user image">
                                  <span class="username">
                                    <a href="#">{{$feed->user->firstname.' '.$feed->user->lastname }} </a>
                                    {{-- <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a> --}}
                                  </span>
                                  <span class="description">Shared at - {{$feed->created_at}}</span>
                                </div>
                                <!-- /.user-block -->
                                <div class="row">
                                  @foreach($feed->feed_topic_medias as $media)
                                    <div class="col-md-3">
                                      @if($media->type == 'image')
                                        <img src="{{$media->media_url}}" style="width:350px; height: auto">
                                      @elseif($media->type == 'video')
                                        <video width="320" height="240" controls>
                                          <source src="{{$media->media_url}}">
                                        </video>
                                      @endif
                                    </div>
                                  @endforeach
                                </div>
                                <div class="p-4">
                                  <p><strong>{{$feed->description}}</strong></p>
                                </div> 
                                @if($feed->feeling)
                                  <div class="d-flex flex-row">
                                    <div class="p-2"><img src="{{$feed->feeling->image}}"></div>
                                    <div class="p-2">{{$feed->feeling->description}}</div>
                                  </div>
                                @endif

                              {{--   <p>
                                  <a href="#" class="link-black text-sm mr-2"><i class="fas fa-share mr-1"></i> Share</a>
                                  <a href="#" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i> Like</a>
                                  <span class="float-right">
                                    <a href="#" class="link-black text-sm">
                                      <i class="far fa-comments mr-1"></i> Comments (5)
                                    </a>
                                  </span>
                                </p>

                                <input class="form-control form-control-sm" type="text" placeholder="Type a comment"> --}}
                              </div>
                              {{-- <div class="row">
                                @foreach($feed->feed_topic_medias as $media)
                                  <div class="col-md-4">
                                    @if($media->type == 'image')
                                      <img src="{{$media->media_url}}">
                                    @elseif($media->type == 'video')
                                      <video width="320" height="240" controls>
                                        <source src="{{$media->media_url}}">
                                      </video>
                                    @endif
                                  </div>
                                @endif
                              </div> --}}
                            </div>
                            <!-- /.card-body -->
                          </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
    <!-- /.content -->
  </div>


@endsection
