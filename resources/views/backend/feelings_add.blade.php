

@extends('backend.layouts.master')

@section('content')
   <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <h3>Feelings Create</h3>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
                <div class="row">
                    <div class="col-12">
                        <!-- /.card -->

                       <div class="card-body">
           <div class="row">
              <div class="col-md-12">

              
              <form action="{{ !$form_data?url('admin/feelings'):url('admin/feelings/'.$form_data->id) }}" method="post" enctype="multipart/form-data" >
              
              @csrf

              @include('backend.layouts.errors')

                <div class="form-group col-md-6">
                  <label>Description</label>
                  <input type="text" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ $form_data->description ?? old('description')}}" required="" placeholder="Description">
                </div>
             

                <div class="form-group col-md-6">
                  <label for="exampleInputFile">Image</label>
                    <input type="hidden" name="img_temp" id="img_temp" value="{{ $form_data->image ?? ''}}">
                    <input type="file" class="form-control @error('image') is-invalid @enderror" name="image" id="img">
                    <br>
                    <img id="image_preview" src="{{ $form_data != ''?$form_data->image:'http://placehold.it/300x300'}}" alt="your image" style="width: 100px; height: 100%"; />
                </div>

                <div class="form-group">
                  <button class="btn btn-lg btn-dark rounded px-5">Submit</button>
                </div>
                
                
              </form>
            </div>
          </div>
        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
    <!-- /.content -->
  </div>


@endsection
