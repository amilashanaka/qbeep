 <nav class="main-header navbar navbar-expand navbar-white navbar-light">
     <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>


  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <span class="brand-text font-weight-light">QBEEP | <small>Admin Panel</small></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
     {{--  <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">

        </div>
        <div class="info">
          <a href="#" class="d-block">Smartway Admin</a>
        </div>
      </div>
 --}}
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

          <li class="nav-item">
            <a href="{{ url('admin') }}" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>Dashboard</p>
            </a>
          </li>

            <li class="nav-header">Users</li>

           {{--  <li class="nav-item ">
                <a href="{{ url('admin/users')}}" class="nav-link">
                    <i class="nav-icon fas fa-users"></i>
                    <p>
                        ADMIN USERS

                    </p>
                </a>

            </li> --}}

            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-users"></i>
                    <p>
                        Qbeep Users
                        <i class="fas fa-angle-left right"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                      <a href="{{ url('admin/influencers') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Influencers</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{ url('admin/app-users') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Users</p>
                      </a>
                    </li>

                </ul>
            </li>


            <li class="nav-header">MASTER</li>


            <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                 Master Settings
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{ url('admin/channels') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Channels</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ url('admin/feelings') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Feelings</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ url('admin/reasons') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Reasons</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ url('admin/feeds') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Feed</p>
                  </a>
                </li>
            </ul>
          </li>




          <li class="nav-header">ADMIN MANAGEMENT</li>


            <li class="nav-item">
              <a href="{{ url('admin/user-types')}}" class="nav-link">
                <i class="nav-icon fas fa-user-cog"></i>
                <p>
                  User Types
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ url('admin/users')}}" class="nav-link">
                <i class="nav-icon fas fa-user-plus"></i>
                <p>
                  Admin Users
                </p>
              </a>
            </li>

          <li class="nav-header"></li>
          <li class="nav-item">
            <a href="{{ route('admin.logout') }}" class="nav-link bg-default">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p class="text">Signout</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
