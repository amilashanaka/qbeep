

@extends('backend.layouts.master')

@section('content')
   <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <h3>Reasons List</h3>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
                <div class="row">
                    <div class="col-12">
                        <!-- /.card -->

                        <div class="card">
                         
                            <!-- /.card-header -->
                            <div class="card-body">
                               <form action="{{ !$form_data?url('admin/reasons'):url('admin/reasons/'.$form_data->id) }}" method="post" >
              
              @csrf

              @include('backend.layouts.errors')

                <div class="form-group col-md-6">
                  <label>Description</label>
                  <input type="text" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ $form_data->description ?? old('description')}}" required="" placeholder="Description">
                </div>

                <div class="form-group">
                  <button class="btn btn-lg btn-dark rounded px-5">Submit</button>
                </div>
                
                
              </form>
                            </div>
                        </div>

                        <div class="card">
                         
                            <!-- /.card-header -->
                            <div class="card-body">
                              <table id="example1" class="table table-bordered table-striped">
                                  <thead>
                                    <tr>
                                      <th>#</th>
                                      <th>Description</th>
                                      <th>Action</th>
                                    </tr>
                                  </thead>
                                  <tbody class="brand-table-body">
                                    @foreach($table_data as $key => $value)
                                      <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $value->description }}</td>
                                        <td><a class="btn btn-md btn-primary" href="{{ url('admin/reasons/'.$value->id) }}">Edit</a></td>
                                      </tr>
                                    @endforeach
                                  
                                  </tbody>
                              </table>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
    <!-- /.content -->
  </div>


@endsection
