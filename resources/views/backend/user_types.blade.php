

@extends('backend.layouts.master')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0"></h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/admin">Home</a></li>
                            <li class="breadcrumb-item active">User Types</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                   <!-- Default box -->
                  <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">Add User Types</h3>
                    </div>
                    <div class="card-body">
                       <div class="row">
                          <div class="col-md-12">

                          
                          <form id="product-form" action="{{ !$form_data ?url('admin/user-types'): url('admin/user-types/'.$form_data->id) }}" method="post" enctype="multipart/form-data" >
                          @csrf

                          @include('backend.layouts.errors')

                            <div class="form-group">
                              <label>Name</label>
                              <input type="text" class="form-control" name="name" value="{{ $form_data->name ?? old('name') }}" required="" placeholder="User Type" required="">
                            </div>
                            <hr>

                            <div class="form-group">
                              <button class="btn btn-md btn-dark px-5"><i class="fa fa-save"></i> Save</button>
                            </div>
                            
                            
                          </form>
                        </div>
                      </div>
                    </div>
                    <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
                  <div class="card">
                      <div class="card-header">
                        <h3 class="card-title">User Types List</h3>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                          <thead>
                          <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Action</th>
                          </tr>
                          </thead>
                          <tbody>
                            @foreach($table_data as $key => $value)
                              <tr>
                                <td>{{ $value->id }}</td>
                                <td>{{ $value->name}}</td>
                                <td><a class="btn btn-md btn-success" href="{{ url('admin/user-types/'.$value->id) }}">Edit</a></td>
                              </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                  </div>
               
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>


@endsection
