<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>QBeep  </title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{asset('frontend/img/favicon.png') }}') }}" rel="icon">
    <link href="{{asset('frontend/img/apple-touch-icon.png') }}') }}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{asset('frontend/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{asset('frontend/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
    <link href="{{asset('frontend/vendor/aos/aos.css') }}" rel="stylesheet">
    <link href="{{asset('frontend/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
    <link href="{{asset('frontend/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet">
    <link href="{{asset('frontend/vendor/glightbox/css/glightbox.min.css') }}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{asset('frontend/css/style.css') }}" rel="stylesheet">

</head>

<body>

<!-- ======= Header ======= -->
<header id="header" class="header fixed-top">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

        <a href="{{url('/')}}" class="logo d-flex align-items-center">
            <img src="{{asset('frontend/img/logo.png') }}" alt="" >
            <span>QBeep</span>
        </a>

        <nav id="navbar" class="navbar">
            <ul>
                <li><a class="nav-link scrollto active" href="#home">Home</a></li>
                <li><a class="nav-link scrollto" href="#about">About</a></li>
                <li><a class="nav-link scrollto" href="#features"> Services  </a></li>
				<li><a class="nav-link scrollto" href="#contact"> Support  </a></li>

            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->

    </div>
</header><!-- End Header -->

<!-- ======= Hero Section ======= -->
<section id="home" class="hero d-flex align-items-center">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 d-flex flex-column justify-content-center">
                <h1 data-aos="fade-up">QBeep's Modern Solution </h1>
                <br>

                <div data-aos="fade-up" data-aos-delay="600">
                    <div class="text-center text-lg-start">
                        <a href="#about" class=" d-inline-flex align-items-center justify-content-center align-self-center">
                            <img src="{{asset('frontend/img/available.png') }}" class="img-fluid" alt="" width="50%">
                        </a>
                    </div>


                </div>
            </div>
            <div class="col-lg-6 hero-img" data-aos="zoom-out" data-aos-delay="200">
                <img src="{{asset('frontend/img/mobiles.png') }}" class="img-fluid" alt="">
            </div>
        </div>
    </div>

</section><!-- End Hero -->

<main id="main">
    <!-- ======= About Section ======= -->
    <section id="about" class="about">

        <div class="container" data-aos="fade-up">
            <div class="row gx-0">

                <div class="col-lg-6 d-flex flex-column justify-content-center" data-aos="fade-up" data-aos-delay="200">
                    <div class="content">
                        <h3 style="font-size: 22px; color: #012970;" >Who We Are</h3>
                        <p>
                            A unique mobile software that allows users to buy and sell on social media while earning and redeeming tier brand loyalty and points at any time and from anywhere. QBeep connects your online and offline stores to increase sales conversions and help you create closer relationships with your customers. It comes with all the necessary capabilities for making sales, tracking performance, and managing clients, orders, and inventory. It is now possible to have a smooth shopping experience at your fingertips!
                        </p>

                    </div>
                </div>

                <div class="col-lg-6 d-flex align-items-center" data-aos="zoom-out" data-aos-delay="200">
                    <img src="{{asset('frontend/img/about.jpg') }}" class="img-fluid" alt="">
                </div>

            </div>
        </div>

    </section><!-- End About Section -->




    <!-- ======= Features Section ======= -->
    <section id="features" class="features">

        <div class="container" data-aos="fade-up">

            <header class="section-header features">
                <h3 style="font-weight: 800; color: #012970;"> Why Choose Us?  </h3>
            </header>

            <div class="row">

                <div class="col-lg-6">
                    <img src="{{asset('frontend/img/features.png') }}" class="img-fluid" alt="">
                </div>

                <div class="col-lg-6 mt-5 mt-lg-0 d-flex">
                    <div class="row align-self-center gy-4">

                        <div class="col-md-6" data-aos="zoom-out" data-aos-delay="200">
                            <div class="feature-box d-flex align-items-center">
                                <i class="bi bi-check"></i>
                                <h3>Unique PERSONAL TAC Login </h3>
                            </div>
                        </div>

                        <div class="col-md-6" data-aos="zoom-out" data-aos-delay="300">
                            <div class="feature-box d-flex align-items-center">
                                <i class="bi bi-check"></i>
                                <h3> Live Streaming Products, Q&A & videos on demand</h3>
                            </div>
                        </div>

                        <div class="col-md-6" data-aos="zoom-out" data-aos-delay="400">
                            <div class="feature-box d-flex align-items-center">
                                <i class="bi bi-check"></i>
                                <h3>No heavy IT investment, staff, and maintenance</h3>
                            </div>
                        </div>

                        <div class="col-md-6" data-aos="zoom-out" data-aos-delay="500">
                            <div class="feature-box d-flex align-items-center">
                                <i class="bi bi-check"></i>
                                <h3>Plug and play </h3>
                            </div>
                        </div>

                        <div class="col-md-6" data-aos="zoom-out" data-aos-delay="600">
                            <div class="feature-box d-flex align-items-center">
                                <i class="bi bi-check"></i>
                                <h3>Stay 24/7 informed and connected</h3>
                            </div>
                        </div>

                        <div class="col-md-6" data-aos="zoom-out" data-aos-delay="700">
                            <div class="feature-box d-flex align-items-center">
                                <i class="bi bi-check"></i>
                                <h3>Build trust and opportunities </h3>
                            </div>
                        </div>

                        <div class="col-md-6" data-aos="zoom-out" data-aos-delay="700">
                            <div class="feature-box d-flex align-items-center">
                                <i class="bi bi-check"></i>
                                <h3>Competitive Edge  </h3>
                            </div>
                        </div>

                        <div class="col-md-6" data-aos="zoom-out" data-aos-delay="700">
                            <div class="feature-box d-flex align-items-center">
                                <i class="bi bi-check"></i>
                                <h3>Safe and Secure Payment Gateway and Infra  </h3>
                            </div>
                        </div>

                        <div class="col-md-6" data-aos="zoom-out" data-aos-delay="700">
                            <div class="feature-box d-flex align-items-center">
                                <i class="bi bi-check"></i>
                                <h3>Buy Now, Pay Later        </h3>
                            </div>
                        </div>

                    </div>
                </div>

            </div> <!-- / row -->



            <!-- Feature Icons -->
            <div class="row feature-icons" data-aos="fade-up">
                <h3>What WE Offer </h3>

                <div class="row">

                    <div class="col-xl-4 text-center" data-aos="fade-right" data-aos-delay="100">
                        <img src="{{asset('frontend/img/features-3.png') }}" class="img-fluid p-4" alt="">
                    </div>

                    <div class="col-xl-8 d-flex content">
                        <div class="row align-self-center gy-4">

                            <div class="col-md-6 icon-box" data-aos="fade-up">
                                <i class="ri-line-chart-line"></i>
                                <div>
                                    <h4>Customers</h4>
                                    <ul>
                                        <li>Anti-aging & Wellness Industry Shoppers </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="100">
                                <i class="ri-stack-line"></i>
                                <div>
                                    <h4>Brands</h4>
                                    <p>
                                    <ul>
                                        <li>  Retail Brands   </li>
                                        <li>  Chain Restaurants </li>
                                    </ul>
                                    </p>
                                </div>
                            </div>

                            <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="200">
                                <i class="ri-brush-4-line"></i>
                                <div>
                                    <h4>Projects </h4>
                                    <p>
                                    <ul>
                                        <li>  Medical based devices   </li>
                                        <li>  Logistic & Distributions  </li>
                                        <li> E-commerce    </li>
                                        <li> Loyalty Programs  </li>
                                        <li>  Social Media Tools   </li>
                                    </ul>
                                    </p>
                                </div>
                            </div>

                            <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="300">
                                <i class="ri-magic-line"></i>
                                <div>
                                    <h4>Offering </h4>
                                    <p>
                                    <ul>
                                        <li>  Internet of Things (IoT) </li>
                                        <li>  Mobile Applications  </li>
                                        <li>  API Integration    </li>
                                        <li>  UI and UX   </li>
                                        <li>  Full Stack Application Development    </li>
                                    </ul>
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div><!-- End Feature Icons -->

        </div>
    </section>
	
	
	
	<!-- ======= Contact Section ======= -->
       <section id="contact" class="contact">

        <div class="container" data-aos="fade-up">
  
          <header class="section-header">
            <p> Support </p>
          </header>
  
          <div class="row">
  
            <div class="col-lg-12">
  
              <div class="row gy-4">
                <div class="col-md-4">
                  <div class="info-box" style="height: 275px;">
                    <i class="bi bi-geo-alt"></i>
                    <h3>Address</h3>
                    <p>B-3A-4, North Point, <br>  Mid Valley City, <br>  No 1, Medan Syed Putra Utama, <br>  59200 Kuala Lumpur,  <br> Malaysia.</p>
                  </div>
                </div>
       
                <div class="col-md-4">
                  <div class="info-box" style="height: 275px;">
                    <i class="bi bi-envelope"></i>
                    <h3>Email Us</h3>
                    <p>enquiry@qbeep.com</p>
                  </div> 
                </div>
                <div class="col-md-4">
                  <div class="info-box" style="height: 275px;">
                    <i class="bi bi-clock"></i>
                    <h3> Operating Hours</h3>
                    <p> Monday - Friday<br>9am – 6pm (+0800 UTC)</p>
                    <p> Public holidays for Malaysia are observed. </p>
                  </div>
                </div>
              </div>
  
            </div>
  
           
          </div>
  
        </div>
  
      </section><!-- End Contact Section -->
  



</main><!-- End #main -->

<!-- ======= Footer ======= -->
<footer id="footer" class="footer">



    <div class="footer-top">
        <div class="container">
            <div class="row gy-4">
                <div class="col-lg-5 col-md-12 footer-info">
                    <a href="index.html" class="logo d-flex align-items-center">
                        <img src="{{asset('frontend/img/logo.png') }}') }}" alt="">
                        <span>QBeep</span>
                    </a>
                    <!-- <div class="social-links mt-3">
                      <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
                      <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
                      <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
                      <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></a>
                    </div> -->
                </div>

                <div class="col-lg-2 col-6 footer-links">
                    <h4>Useful Links</h4>
                    <ul>
                        <li><i class="bi bi-chevron-right"></i> <a href="{{url('/')}}">Home</a></li>
                        <li><i class="bi bi-chevron-right"></i> <a href="{{url('/privacy')}}">Privacy policy</a></li>
                    </ul>
                </div>

                <!-- <div class="col-lg-2 col-6 footer-links">
                  <h4>Our Services</h4>
                  <ul>
                    <li><i class="bi bi-chevron-right"></i> <a href="#">Web Design</a></li>
                    <li><i class="bi bi-chevron-right"></i> <a href="#">Web Development</a></li>
                    <li><i class="bi bi-chevron-right"></i> <a href="#">Product Management</a></li>
                    <li><i class="bi bi-chevron-right"></i> <a href="#">Marketing</a></li>
                    <li><i class="bi bi-chevron-right"></i> <a href="#">Graphic Design</a></li>
                  </ul>
                </div>

                <div class="col-lg-3 col-md-12 footer-contact text-center text-md-start">
                  <h4>Contact Us</h4>
                  <p>
                    A108 Adam Street <br>
                    New York, NY 535022<br>
                    United States <br><br>
                    <strong>Phone:</strong> +1 5589 55488 55<br>
                    <strong>Email:</strong> info@example.com<br>
                  </p>

                </div> -->

            </div>
        </div>
    </div>

    <div class="container">
        <div class="copyright">
            &copy; Copyright <strong><span>QBeep</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
            Designed by <a href="#">QBeep </a>
        </div>
    </div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<!-- Vendor JS Files -->
<script src="{{asset('frontend/vendor/bootstrap/js/bootstrap.bundle.js') }}"></script>
<script src="{{asset('frontend/vendor/aos/aos.js') }}"></script>
<script src="{{asset('frontend/vendor/php-email-form/validate.js') }}"></script>
<script src="{{asset('frontend/vendor/swiper/swiper-bundle.min.js') }}"></script>
<script src="{{asset('frontend/vendor/purecounter/purecounter.js') }}"></script>
<script src="{{asset('frontend/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
<script src="{{asset('frontend/vendor/glightbox/js/glightbox.min.js') }}"></script>

<!-- Template Main JS File -->
<script src="{{asset('frontend/js/main.js') }}"></script>

</body>

</html>
