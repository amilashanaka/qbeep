<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>QBeep - Policy  </title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{asset('frontend/img/favicon.png') }}" rel="icon">
  <link href="{{asset('frontend/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{asset('frontend/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{asset('frontend/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
  <link href="{{asset('frontend/vendor/aos/aos.css') }}" rel="stylesheet">
  <link href="{{asset('frontend/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
  <link href="{{asset('frontend/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet">
  <link href="{{asset('frontend/vendor/glightbox/css/glightbox.min.css') }}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{asset('frontend/css/style.css') }}" rel="stylesheet">

</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="header fixed-top">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

      <a href="{{url('/')}}" class="logo d-flex align-items-center">
        <img src="{{asset('frontend/img/logo.png') }}" alt="">
        <span>QBeep </span>
      </a>

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto " href="{{url('/')}}">Home</a></li>

        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="{{url('/')}}">Home</a></li>
          <li><a href="{{url('/privacy')}}">Policy and Privacy </a></li>
        </ol>
        <h2>Privacy Policy </h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Blog Single Section ======= -->
    <section id="blog" class="blog">
      <div class="container" data-aos="fade-up">

        <div class="row">

          <div class="col-lg-12 entries">

            <article class="entry entry-single">

              <div class="entry-content">
                <p>
                  Thank you for choosing to be part of our community at QBEEP INTELLIGENT SYSTEMS SDN. BHD. ("Company," "we," "us," or "our"). We are committed to protecting your personal information and your right to privacy. If you have any questions or concerns about this privacy notice or our practices with regard to your personal information, please contact us at enquiry@qbeep.com . This privacy notice describes how we might use your information if you:
                </p>

                <h4>
                  Download and use our mobile application—QBeep.
                  Engage with us in other related ways ― including any sales, marketing, or events.
                </h4>

                <p>
                  In this privacy notice, if we refer to: "App," we are referring to any application of ours that link to this policy, including any listed above "Services," we are referring to our App, and other related services, including any sales, marketing, or events The purpose of this privacy notice is to explain to you in the clearest way possible what information we collect, how we use it, and what rights you have in relation to it. If there are any terms in this privacy notice that you do not agree with, please discontinue use of our Services immediately. 
                </p>
                <p><strong>Please read this privacy notice carefully, as it will help you understand what we do with the information that we collect.  
                </strong> </p>

    
                  <p>
                    TABLE OF CONTENTS 
                    <ol>
                      <li> WHAT INFORMATION DO WE COLLECT? </li>
                      <li> HOW DO WE USE YOUR INFORMATION?  </li>
                      <li> WILL YOUR INFORMATION BE SHARED WITH ANYONE?   </li>
                      <li> DO WE USE COOKIES AND OTHER TRACKING TECHNOLOGIES?  </li>
                      <li> HOW LONG DO WE KEEP YOUR INFORMATION?  </li>
                      <li> HOW DO WE KEEP YOUR INFORMATION SAFE?   </li>
                      <li> WHAT ARE YOUR PRIVACY RIGHTS?   </li>
                      <li> CONTROLS FOR DO-NOT-TRACK FEATURES  </li>
                      <li> DO CALIFORNIA RESIDENTS HAVE SPECIFIC PRIVACY RIGHTS?  </li>
                      <li> DO WE MAKE UPDATES TO THIS NOTICE?  </li>
                      <li>  HOW CAN YOU CONTACT US ABOUT THIS NOTICE?   </li>
                      <li> HOW CAN YOU REVIEW, UPDATE OR DELETE THE DATA WE COLLECT FROM YOU?    </li>
                    </ol>


                    <br><!--[if !supportLists]--><b>1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b><!--[endif]--><b>WHAT INFORMATION DO WE COLLECT?</b><b>&nbsp;</b>
                    <br>
                    <br>&nbsp;<b>Personal information you disclose to us</b>&nbsp;<br>
                    <br>In Short: We collect personal information
                    that you provide to us.
                    &nbsp;
                    We collect personal information that you
                    voluntarily provide to us when you register on the App, express an interest in
                    obtaining information about us or our products and Services, when you
                    participate in activities on the App or otherwise when you contact us.
                    &nbsp;
                    The personal information that we collect
                    depends on the context of your interactions with us and the App, the choices
                    you make and the products and features you use. The personal information we
                    collect may include the following:&nbsp; &nbsp;<br>
                    <br><b>Personal Information Provided by You</b>
                    <br>
                    <br>We collect names; phone numbers ; email addresses; mailing addresses; job titles;
                    usernames; passwords; contact preferences; contact or authentication data; billing
                    addresses; debit/credit card numbers; and other similar information.&nbsp; &nbsp;<br>
                    <br><b>Payment Data</b>
                    <br>
                    <br>We may collect data
                    necessary to process your payment if you make purchases, such as your payment
                    instrument number (such as a credit card number), and the security code
                    associated with your payment instrument. All payment data is stored by Razer
                    Merchant Service. You may find their privacy notice link(s) here: <a href="https://merchant.razer.com/v3/privacy-policy/" class="u-active-none u-border-none u-btn u-button-style u-hover-none u-none u-text-palette-1-base u-btn-2">https://merchant.razer.com/v3/privacy-policy/</a>.
                    &nbsp;
                    All personal information that you provide
                    to us must be true, complete and accurate, and you must notify us of any
                    changes to such personal information.&nbsp; &nbsp;<br>
                    <br><b>Information automatically collected</b>&nbsp;<br>
                    <br>
                    <span style="font-weight: 700;"><b>In Short</b>:&nbsp;
                    </span>
                    <br>
                    <br>Some information — such as
                    your Internet Protocol (IP) address and/or browser and device characteristics —
                    is collected automatically when you visit our App.
                    &nbsp;
                    We automatically collect certain
                    information when you visit, use or navigate the App. This information does not
                    reveal your specific identity (like your name or contact information) but may
                    include device and usage information, such as your IP address, browser and
                    device characteristics, operating system, language preferences, referring URLs,
                    device name, country, location, information about how and when you use our App
                    and other technical information. This information is primarily needed to
                    maintain the security and operation of our App, and for our internal analytics
                    and reporting purposes.
                    &nbsp;
                    Like many businesses, we also collect
                    information through cookies and similar technologies.&nbsp;&nbsp;<br>
                    <br>The information we collect includes:&nbsp; &nbsp;<br>
                    <br>
                    <span style="font-weight: 700;">Log and Usage Data&nbsp;</span>
                    <br>
                    <br>Log and usage data is
                    service-related, diagnostic, usage and performance information our servers
                    automatically collect when you access or use our App and which we record in log
                    files. Depending on how you interact with us, this log data may include your IP
                    address, device information, browser type and settings and information about
                    your activity in the App (such as the date/time stamps associated with your
                    usage, pages and files viewed, searches and other actions you take such as
                    which features you use), device event information (such as system activity,
                    error reports (sometimes called 'crash dumps') and hardware settings).&nbsp; &nbsp;<br>
                    <br>
                    <span style="font-weight: 700;">Device Data</span>
                    <br>
                    <br>We collect device data such as
                    information about your computer, phone, tablet or other device you use to
                    access the App. Depending on the device used, this device data may include
                    information such as your IP address (or proxy server), device and application
                    identification numbers, location, browser type, hardware model Internet service
                    provider and/or mobile carrier, operating system and system configuration information.&nbsp;&nbsp;<br>
                    <br>
                    <span style="font-weight: 700;">Location Data</span>
                    <br>
                    <br>We collect location data such as
                    information about your device's location, which can be either precise or
                    imprecise. How much information we collect depends on the type and settings of
                    the device you use to access the App. For example, we may use GPS and other
                    technologies to collect geolocation data that tells us your current location
                    (based on your IP address). You can opt out of allowing us to collect this
                    information either by refusing access to the information or by disabling your
                    Location setting on your device. Note however, if you choose to opt out, you
                    may not be able to use certain aspects of the Services.
                    &nbsp; <b>&nbsp;</b><b>&nbsp;</b>
                    <br>
                    <br><b>Information collected through
                      our App</b>&nbsp;<br>
                      <br>
                      <span style="font-weight: 700;">In Short:</span>
                      <br>
                      <br>We collect information
                      regarding your geolocation, mobile device, push notifications, when you use our
                      App.
                      If you use our App, we also
                      collect the following information:&nbsp;<br>
                      <br>
                      <span style="font-weight: 700;">Geolocation Information</span>
                      <br>
                      <br>We may request access
                      or permission to and track location-based information from your mobile device,
                      either continuously or while you are using our App, to provide certain
                      location-based services. If you wish to change our access or permissions, you
                      may do so in your device's settings.&nbsp;<br>
                      <br>
                      <span style="font-weight: 700;">Mobile Device Access</span>
                      <br>
                      <br>We may request access or
                      permission to certain features from your mobile device, including your mobile
                      device's bluetooth , calendar, camera, contacts, microphone, reminders,
                      sensors, sms messages, social media accounts, storage, and other features. If
                      you wish to change our access or permissions, you may do so in your device's
                      settings.&nbsp;<br>
                      <br>
                      <span style="font-weight: 700;">Mobile Device Data</span>
                      <br>
                      <br>We automatically collect
                      device information (such as your mobile device ID, model and manufacturer),
                      operating system, version information and system configuration information,
                      device and application identification numbers, browser type and version,
                      hardware model Internet service provider and/or mobile carrier, and Internet
                      Protocol (IP) address (or proxy server). If you are using our App, we may also
                      collect information about the phone network associated with your mobile device,
                      your mobile device’s operating system or platform, the type of mobile device
                      you use, your mobile device’s unique device ID and information about the
                      features of our App you accessed.<br>
                      <br>
                      <span style="font-weight: 700;">Push Notifications</span>
                      <br>
                      <br>We may request to send you
                      push notifications regarding your account or certain features of the App. If
                      you wish to opt-out from receiving these types of communications, you may turn
                      them off in your device's settings.
                      &nbsp;
                      This information is primarily needed to
                      maintain the security and operation of our App, for troubleshooting and for our
                      internal analytics and reporting purposes.&nbsp; &nbsp;<br>
                      <br><b>Information collected from other sources</b>&nbsp;&nbsp;<br>
                      <br>
                      <span style="font-weight: 700;">In Short:</span>
                      <br>
                      <br>We may collect limited data from
                      public databases, marketing partners, and other outside sources.
                      &nbsp;
                      In order to enhance our ability to provide
                      relevant marketing, offers and services to you and update our records, we may
                      obtain information about you from other sources, such as public databases,
                      joint marketing partners, affiliate programs, data providers, as well as from other
                      third parties. This information includes mailing addresses, job titles, email
                      addresses, phone numbers, intent data (or user behavior data), Internet
                      Protocol (IP) addresses, social media profiles, social media URLs and custom
                      profiles, for purposes of targeted advertising and event promotion.&nbsp;<br>
                      <br><b>2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b><!--[endif]--><b>HOW DO WE USE YOUR INFORMATION?</b>&nbsp; &nbsp;<br>
                      <br>
                      <span style="font-weight: 700;">In Short:&nbsp;</span>
                      <br>
                      <br>We process your information for
                      purposes based on legitimate business interests, the fulfillment of our
                      contract with you, compliance with our legal obligations, and/or your consent. We use personal information collected via
                      our App for a variety of business purposes described below. We process your
                      personal information for these purposes in reliance on our legitimate business
                      interests, in order to enter into or perform a contract with you, with your
                      consent, and/or for compliance with our legal obligations. We indicate the
                      specific processing grounds we rely on next to each purpose listed below. We use the information we collect or
                      receive:
                      &nbsp; <br>
                      <br><b>To facilitate account creation and logon
                        process</b>
                        <br>
                        <br>If you choose to link your account with us to a third-party
                        account (such as your Google or Facebook account), we use the information you
                        allowed us to collect from those third parties to facilitate account creation
                        and logon process for the performance of the contract.&nbsp;<br>
                        <br><b>Request feedback</b>
                        <br>
                        <br>We may use your information
                        to request feedback and to contact you about your use of our App.&nbsp;<br>
                        <br><b>To enable user-to-user communications</b>
                        <br>
                        <br>We
                        may use your information in order to enable user-to-user communications with
                        each user's consent.&nbsp;<br>
                        <br><b>To manage user accounts</b>
                        <br>
                        <br>We may use your
                        information for the purposes of managing our account and keeping it in working
                        order.&nbsp;<br>
                        <br><b>To send administrative information to you</b>
                        <br>
                        <br>We may use your personal information to send you product, service and new
                        feature information and/or information about changes to our terms, conditions,
                        and policies.&nbsp;<br>
                        <br><b>To protect our Services</b>
                        <br>
                        <br>We may use your
                        information as part of our efforts to keep our App safe and secure (for
                        example, for fraud monitoring and prevention).&nbsp;&nbsp;<br>
                        <br>
                        <span style="font-weight: 400;"><b>To enforce our terms, conditions and policies
                          for business purposes, to comply with legal and regulatory requirements or in
                          connection with our contract.</b>&nbsp;
                        </span>
                        <br>
                        <br><b>To respond to legal requests and prevent harm</b>
                        <br>
                        <br>If we receive a subpoena or other legal request, we may need to inspect the
                        data we hold to determine how to respond.&nbsp;&nbsp;&nbsp;&nbsp;<br>
                        <br><b>Fulfill
                          and manage your orders</b>
                          <br>
                          <br>We may use your information to fulfill and manage
                          your orders, payments, returns, and exchanges made through the App.<br>
                          <br><b>Administer prize draws and competitions</b>
                          <br>
                          <br>We may use your information to administer prize draws and competitions when you
                          elect to participate in our competitions.&nbsp;<br>
                          <br><b>To deliver and facilitate delivery of
                            services to the user</b>
                            <br>
                            <br>We may use your information to provide you with the
                            requested service.&nbsp;&nbsp;<br>
                            <br><b>To respond to user inquiries/offer support to
                              users</b>
                              <br>
                              <br>We may use your information to respond to your inquiries and solve
                              any potential issues you might have with the use of our Services.<br>
                              <br><b>To send you marketing and promotional
                                communications</b>
                                <br>
                                <br>We and/or our third-party marketing partners may use the
                                personal information you send to us for our marketing purposes, if this is in
                                accordance with your marketing preferences. For example, when expressing an
                                interest in obtaining information about us or our App, subscribing to marketing
                                or otherwise contacting us, we will collect personal information from you. You
                                can opt-out of our marketing emails at any time (see the "WHAT AREYOUR
                                PRIVACY RIGHTS?" below).
                                &nbsp;
                                &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<!--[endif]-->
                                <br>
                                <br><b>Deliver targeted advertising to you</b>
                                <br>
                                <br>We
                                may use your information to develop and display personalized content and
                                advertising (and work with third parties who do so) tailored to your interests
                                and/or location and to measure its effectiveness.&nbsp; &nbsp;<br>
                                <br><b>For other business purposes</b>
                                <br>
                                <br>We may use your information for other business
                                purposes, such as data analysis, identifying usage trends, determining the effectiveness
                                of our promotional campaigns and to evaluate and improve our App, products,
                                marketing and your experience. We may use and store this information in
                                aggregated and anonymized form so that it is not associated with individual end
                                users and does not include personal information. We will not use identifiable
                                personal information without your consent.<br>
                                <br><b>3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b><!--[endif]--><b>WILL YOUR INFORMATION BE SHARED WITH
                                  ANYONE?</b>&nbsp;<br>
                                  <br>
                                  <span style="font-weight: 700;">In Short:<br>
                                  </span>
                                  <br>We only share
                                  information with your consent, to comply with laws, to provide you with services, to protect your rights,
                                  or to fulfill business obligations. We may
                                  process or share your data that we hold based on the following legal basis:&nbsp;<br>
                                  <br><b>Consent:&nbsp;</b>
                                  <br>
                                  <br>We may process your data if you
                                  have given us specific consent to use your personal information for a specific
                                  purpose. <b>&nbsp;</b> &nbsp; &nbsp; &nbsp; &nbsp;<!--[endif]-->
                                  <br><b>Legitimate Interests:&nbsp;</b>
                                  <br>
                                  <br>We may process your
                                  data when it is reasonably necessary to achieve our legitimate business
                                  interests. <b>&nbsp;</b>&nbsp;<br>
                                  <br><b>Performance of a Contract:&nbsp;</b>
                                  <br>
                                  <br>Where we have
                                  entered into a contract with you, we may process your personal information to
                                  fulfill the terms of our contract.&nbsp;&nbsp;<br>
                                  <br><b>Legal Obligations:&nbsp;</b>
                                  <br>
                                  <br>We may disclose your
                                  information where we are legally required to do so in order to comply with
                                  applicable law, governmental requests, a judicial proceeding, court order, or
                                  legal process, such as in response to a court order or a subpoena (including in
                                  response to public authorities to meet national security or law enforcement
                                  requirements).&nbsp; &nbsp;<br>
                                  <br><b>Vital Interests:&nbsp;</b>
                                  <br>
                                  <br>We may disclose your
                                  information where we believe it is necessary to investigate, prevent, or take
                                  action regarding potential violations of our policies, suspected fraud,
                                  situations involving potential threats to the safety of any person and illegal
                                  activities, or as evidence in litigation in which we are involved.
                                  More specifically, we may need to process your data or share
                                  your personal information in the following situations:<br>
                                  <br><b>Business Transfers</b>
                                  <br>
                                  <br>We may share or
                                  transfer your information in connection with, or during negotiations of, any
                                  merger, sale of company assets, financing, or acquisition of all or a portion
                                  of our business to another company.&nbsp;<br>
                                  <br><b>Google Maps Platform APIs</b>
                                  <br>
                                  <br>We may share
                                  your information with certain Google Maps Platform APIs (e.g., Google Maps API,
                                  Place API). To find out more about Google’s Privacy Policy, please refer to
                                  this link <a href="https://policies.google.com/privacy?hl=en-MY&amp;fg=1" class="u-active-none u-border-none u-btn u-button-style u-hover-none u-none u-text-palette-1-base u-btn-3">https://policies.google.com/privacy?hl=en-MY&amp;fg=1</a>.
                                  We obtain and store on your device ('cache') your location. You may revoke your
                                  consent anytime by contacting us at the contact details provided at the end of
                                  this document.<br>
                                  <br><b>Offer Wall</b>
                                  <br>
                                  <br>Our App may display a
                                  third-party hosted "offer wall." Such an offer wall allows
                                  third-party advertisers to offer virtual currency, gifts, or other items to
                                  users in return for the acceptance and completion of an advertisement offer.
                                  Such an offer wall may appear in our App and be displayed to you based on
                                  certain data, such as your geographic area or demographic information. When you
                                  click on an offer wall, you will be brought to an external website belonging to
                                  other persons and will leave our App. A unique identifier, such as your user
                                  ID, will be shared with the offer wall provider in order to prevent fraud and properly
                                  credit your account with the relevant reward. Please note that we do not
                                  control third-party websites and have no responsibility in relation to the
                                  content of such websites. The inclusion of a link towards a third-party website
                                  does not imply an endorsement by us of such website. Accordingly, we do not
                                  make any warranty regarding such third-party websites and we will not be liable
                                  for any loss or damage caused by the use of such websites. In addition, when
                                  you access any third-party website, please understand that your rights while
                                  accessing and using those websites will be governed by the privacy notice and
                                  terms of service relating to the use of those websites.&nbsp;&nbsp;&nbsp;<br>
                                  <br><b>4. DO WE USE COOKIES AND OTHER TRACKING TECHNOLOGIES?</b>&nbsp;<br>
                                  <br>
                                  <span style="font-weight: 700;">In Short:&nbsp;</span>
                                  <br>
                                  <br>We may use cookies and other tracking technologies
                                  to collect and store your information.
                                  We may use cookies and similar tracking technologies (like
                                  web beacons and pixels) to access or store information. Specific information
                                  about how we use such technologies and how you can refuse certain cookies is
                                  set out in our Cookie Notice.&nbsp;&nbsp;<br>
                                  <br><b>5. HOW LONG DO WE KEEP YOUR INFORMATION?</b>&nbsp;<br>
                                  <br>
                                  <span style="font-weight: 700;">In Short:</span>&nbsp;<br>
                                  <br>We keep your information for as long as necessary
                                  to fulfill the purposes outlined in this privacy notice unless otherwise
                                  required by law. We will only keep your personal information for as long as it
                                  is necessary for the purposes set out in this privacy notice, unless a longer
                                  retention period is required or permitted by law (such as tax, accounting or
                                  other legal requirements). No purpose in this notice will require us keeping
                                  your personal information for longer than the period of time in which users
                                  have an account with us.
                                  When we have no ongoing legitimate business need
                                  to process your personal information, we will either delete or anonymize such information,
                                  or, if this is not possible (for example, because your personal information has
                                  been stored in backup archives), then we will securely store your personal
                                  information and isolate it from any further processing until deletion is
                                  possible.<br>
                                  <br><b>6. HOW DO WE KEEP YOUR INFORMATION SAFE?</b>&nbsp;<br>
                                  <br>
                                  <span style="font-weight: 700;">In Short:&nbsp;</span>
                                  <br>
                                  <br>We aim to protect your personal information
                                  through a system of organizational and technical security measures.
                                  We have implemented appropriate technical and organizational
                                  security measures designed to protect the security of any personal information
                                  we process. However, despite our safeguards and efforts to secure your
                                  information, no electronic transmission over the Internet or information
                                  storage technology can be guaranteed to be 100% secure, so we cannot promise or
                                  guarantee that hackers, cybercriminals, or other unauthorized third parties
                                  will not be able to defeat our security, and improperly collect, access, steal,
                                  or modify your information. Although we will do our best to protect your
                                  personal information, transmission of personal information to and from our App
                                  is at your own risk. You should only access the App within a secure
                                  environment.&nbsp;&nbsp;<br>
                                  <br><b>7. WHAT ARE YOUR PRIVACY RIGHTS?</b>&nbsp;<br>
                                  <br>
                                  <span style="font-weight: 700;">In Short:</span>
                                  <br>
                                  <br>You may review, change, or terminate your account
                                  at any time.
                                  If you are a resident in the EEA or UK and you believe we
                                  are unlawfully processing your personal information, you also have the right to
                                  complain to your local data protection supervisory authority. You can find
                                  their contact details here:&nbsp;<br>
                                  <br>
                                  <a href="https://ec.europa.eu/justice/data-protection/bodies/authorities/index_en.htm." class="u-active-none u-border-none u-btn u-button-style u-hover-none u-none u-text-palette-1-base u-btn-4">https://ec.europa.eu/justice/data-protection/bodies/authorities/index_en.htm.</a>&nbsp;<br>
                                  <br>If you are a resident in Switzerland, the contact details
                                  for the data protection authorities are available here: <a href="https://www.edoeb.admin.ch/edoeb/en/home.html" class="u-active-none u-border-none u-btn u-button-style u-hover-none u-none u-text-palette-1-base u-btn-5">https://www.edoeb.admin.ch/edoeb/en/home.html</a>.&nbsp; &nbsp;<br>
                                  <br><b>Account Information</b>&nbsp;<br>
                                  <br>If you would at any time like to review or change the
                                  information in your account or terminate your account, you can:
                                  Log in to your account settings and update your user
                                  account.
                                  Upon your request to terminate your account, we will
                                  deactivate or delete your account and information from our active databases.
                                  However, we may retain some information in our files to prevent fraud,
                                  troubleshoot problems, assist with any investigations, enforce our Terms of Use
                                  and/or comply with applicable legal requirements.&nbsp;<br>
                                  <br><b><u>Cookies and similar technologies</u></b><u>:</u>&nbsp;<br>
                                  <br>Most
                                  Web browsers are set to accept cookies by default. If you prefer, you can
                                  usually choose to set your browser to remove cookies and to reject cookies. If
                                  you choose to remove cookies or reject cookies, this could affect certain
                                  features or services of our App. To opt-out of interest-based advertising by
                                  advertisers on our App visit <a href="http://www.aboutads.info/choices/" class="u-active-none u-border-none u-btn u-button-style u-hover-none u-none u-text-palette-1-base u-btn-6">http://www.aboutads.info/choices/</a>.&nbsp; &nbsp;<br>
                                  <br><b><u>Opting out of email marketing:&nbsp;</u></b>
                                  <br>
                                  <br>You can
                                  unsubscribe from our marketing email list at any time by clicking on the
                                  unsubscribe link in the emails that we send or by contacting us using the
                                  details provided below. You will then be removed from the marketing email list
                                  — however, we may still communicate with you, for example to send you
                                  service-related emails that are necessary for the administration and use of
                                  your account, to respond to service requests, or for other non-marketing
                                  purposes. To otherwise opt-out, you may:&nbsp;<br>
                                  <br>Access your account settings and update your
                                  preferences.&nbsp;&nbsp;<br>
                                  <br><b>8. CONTROLS FOR DO-NOT-TRACK FEATURES</b>&nbsp;<br>
                                  <br>Most web browsers and some mobile operating systems and
                                  mobile applications include a Do-Not-Track ("DNT") feature or setting
                                  you can activate to signal your privacy preference not to have data about your
                                  online browsing activities monitored and collected. At this stage no uniform
                                  technology standard for recognizing and implementing DNT signals has been
                                  finalized. As such, we do not currently respond to DNT browser signals or any
                                  other mechanism that automatically communicates your choice not to be tracked
                                  online. If a standard for online tracking is adopted that we must follow in the
                                  future, we will inform you about that practice in a revised version of this
                                  privacy notice.&nbsp;&nbsp;<br>
                                  <br><b>9. DO CALIFORNIA RESIDENTS HAVE SPECIFIC PRIVACY RIGHTS?</b>&nbsp;<br>
                                  <br>
                                  <span style="font-weight: 700;">In Short:&nbsp;</span>
                                  <br>
                                  <br>Yes, if you are a resident of California, you are
                                  granted specific rights regarding access to your personal information.
                                  California Civil Code Section 1798.83, also known as the
                                  "Shine The Light" law, permits our users who are California residents
                                  to request and obtain from us, once a year and free of charge, information
                                  about categories of personal information (if any) we disclosed to third parties
                                  for direct marketing purposes and the names and addresses of all third parties
                                  with which we shared personal information in the immediately preceding calendar
                                  year. If you are a California resident and would like to make such a request,
                                  please submit your request in writing to us using the contact information
                                  provided below.
                                  If you are under 18 years of age, reside in California, and
                                  have a registered account with the App, you have the right to request removal
                                  of unwanted data that you publicly post on the App. To request removal of such
                                  data, please contact us using the contact information provided below, and
                                  include the email address associated with your account and a statement that you
                                  reside in California. We will make sure the data is not publicly displayed on
                                  the App, but please be aware that the data may not be completely or
                                  comprehensively removed from all our systems (e.g. backups, etc.).&nbsp;&nbsp;<br>
                                  <br>
                                  <span style="font-weight: 700;">10.&nbsp;</span><b>DO WE MAKE UPDATES TO THIS NOTICE?</b>
                                  <br>
                                  <br>
                                  <span style="font-weight: 700;">In Short:</span>
                                  <br>
                                  <br>Yes, we will update this notice as necessary to
                                  stay compliant with relevant laws.
                                  We may update this privacy notice from time to time. The
                                  updated version will be indicated by an updated "Revised" date and
                                  the updated version will be effective as soon as it is accessible. If we<b></b>make
                                  material changes to this privacy notice, we may notify you either by
                                  prominently posting a notice of such changes or by directly sending you a
                                  notification. We encourage you to review this privacy notice frequently to be
                                  informed of how we are protecting your information.&nbsp;&nbsp;<br>
                                  <br><b>11. HOW CAN YOU CONTACT US ABOUT THIS NOTICE?</b>
                                  <br>
                                  <br>If you have questions or comments about this notice, you may
                                  email us at enquiry@qbeep.com.<br>
                                  <br><b>12. HOW CAN YOU REVIEW, UPDATE, OR DELETE THE DATA WE
                                    COLLECT FROM YOU?</b>&nbsp;<br>
                                    <br>Based on the applicable laws of your country,
                                    you may have the right to request access to the personal information we collect
                                    from you, change that information, or delete it in some circumstances. To
                                    request to review, update, or delete your personal information, please email us
                                    at <a href="customercare@qbeep.com" class="u-active-none u-border-none u-btn u-button-style u-hover-none u-none u-text-palette-1-base u-btn-7">customercare@qbeep.com</a>
                                    <br>
                                  </p>
     

            

              </div>

            </article><!-- End blog entry -->

         
           
          </div><!-- End blog entries list -->

        </div>

      </div>
    </section><!-- End Blog Single Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer" class="footer">

  

    <div class="footer-top">
      <div class="container">
        <div class="row gy-4">
          <div class="col-lg-5 col-md-12 footer-info">
            <a href="index.html" class="logo d-flex align-items-center">
              <img src="{{asset('frontend/img/logo.png') }}" alt="">
              <span>QBeep</span>
            </a>
            <!-- <div class="social-links mt-3">
              <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
              <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
              <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
              <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></a>
            </div> -->
          </div>

          <div class="col-lg-2 col-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bi bi-chevron-right"></i> <a href="{{url('/')}}">Home</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="{{url('/privacy')}}">Privacy policy</a></li>
            </ul>
          </div>

          <!-- <div class="col-lg-2 col-6 footer-links">
            <h4>Our Services</h4>
            <ul>
              <li><i class="bi bi-chevron-right"></i> <a href="#">Web Design</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#">Web Development</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#">Product Management</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#">Marketing</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="#">Graphic Design</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-12 footer-contact text-center text-md-start">
            <h4>Contact Us</h4>
            <p>
              A108 Adam Street <br>
              New York, NY 535022<br>
              United States <br><br>
              <strong>Phone:</strong> +1 5589 55488 55<br>
              <strong>Email:</strong> info@example.com<br>
            </p>

          </div> -->

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>QBeep</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        Designed by <a href="#">QBeep </a>
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{asset('frontend/vendor/bootstrap/js/bootstrap.bundle.js') }}"></script>
  <script src="{{asset('frontend/vendor/aos/aos.js') }}"></script>
  <script src="{{asset('frontend/vendor/php-email-form/validate.js') }}"></script>
  <script src="{{asset('frontend/vendor/swiper/swiper-bundle.min.js') }}"></script>
  <script src="{{asset('frontend/vendor/purecounter/purecounter.js') }}"></script>
  <script src="{{asset('frontend/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
  <script src="{{asset('frontend/vendor/glightbox/js/glightbox.min.js') }}"></script>

  <!-- Template Main JS File -->
  <script src="{{asset('frontend/js/main.js') }}"></script>

</body>

</html>