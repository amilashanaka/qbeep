<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\ApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


// Route::group([ 'prefix' => 'auth'], function (){

//     Route::group(['middleware' => ['guest:api']], function () {
//         Route::post('login', [AuthController::class, 'login']);
//     });

//     Route::group(['middleware' => 'auth:api'], function() {
//         Route::get('logout', [AuthController::class, 'logout']);
//         Route::get('get-channels', [ApiController::class, 'getChannels']);
//     });
// });



Route::group(['middleware' => ['guest:api']], function () {
    Route::post('login', [AuthController::class, 'login']);
});


Route::group(['middleware' => 'auth:api'], function() {
    Route::get('logout', [AuthController::class, 'logout']);
    Route::get('channels', [ApiController::class, 'getChannels']);
    Route::get('feeds', [ApiController::class, 'getFeeds']);
    Route::post('feeds', [ApiController::class, 'storeFeeds']);
    Route::post('likes', [ApiController::class, 'storeLikes']);

    Route::get('comments', [ApiController::class, 'getComments']);
    Route::post('comments', [ApiController::class, 'storeComments']);

    Route::post('flag-comment', [ApiController::class, 'flagComments']);

});

Route::get('feelings', [ApiController::class, 'getFeelings']);
Route::get('reasons', [ApiController::class, 'getReasons']);
Route::get('users', [AuthController::class, 'getUsers']);
Route::post('check_text', [ApiController::class, 'check_text']);


