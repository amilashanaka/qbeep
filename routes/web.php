<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\UserTypeController;
use App\Http\Controllers\Admin\ChannelController;
use App\Http\Controllers\Admin\FeelingController;
use App\Http\Controllers\Admin\FeedController;
use App\Http\Controllers\Admin\ReasonController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/welcome', function () {
//     return view('certificate');
// });


Route::get('/', [FrontController::class, 'index'])->name('home');
Route::get('/privacy', [FrontController::class, 'privacy']);


Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function()
{
    Route::prefix('/admin')->group(function () {
        Route::get('/', [AdminController::class, 'index'])->name('admin.dashboard');
        Route::get('/dashboard', [AdminController::class, 'index'])->name('admin.dashboard');
        Route::post('/logout',[AdminController::class, 'logout'])->name('admin.logout');
        Route::get('/logout',[AdminController::class, 'logout'])->name('admin.logout');

        Route::get('/user-types',[UserTypeController::class, 'index']);
        Route::post('/user-types',[UserTypeController::class, 'store']);
        Route::get('/user-types/{id}',[UserTypeController::class, 'edit']);
        Route::post('/user-types/{id}',[UserTypeController::class, 'update']);

        Route::get('/channels',[ChannelController::class, 'index']);
        Route::get('/channels/create',[ChannelController::class, 'create']);
        Route::post('/channels',[ChannelController::class, 'store']);
        Route::get('/channels/{id}',[ChannelController::class, 'edit']);
        Route::post('/channels/{id}',[ChannelController::class, 'update']);
        Route::post('/channels/images/add', [ChannelController::class, 'addImages']);

        

        Route::get('/feelings',[FeelingController::class, 'index']);
        Route::get('/feelings/create',[FeelingController::class, 'create']);
        Route::post('/feelings',[FeelingController::class, 'store']);
        Route::get('/feelings/{id}',[FeelingController::class, 'edit']);
        Route::post('/feelings/{id}',[FeelingController::class, 'update']);

        Route::get('/reasons',[ReasonController::class, 'index']);
        Route::post('/reasons',[ReasonController::class, 'store']);
        Route::get('/reasons/{id}',[ReasonController::class, 'edit']);
        Route::post('/reasons/{id}',[ReasonController::class, 'update']);

        Route::get('/feeds',[FeedController::class, 'index']);
        Route::get('/feeds/{id}/view',[FeedController::class, 'view']);
        
        Route::get('/distance',[AdminController::class, 'distance']);

        Route::get('/influencers',[UserController::class, 'influencers']);
        Route::get('/app-users',[UserController::class, 'appUsers']);

        Route::get('users', [UserController::class, 'index']);
        Route::post('users', [UserController::class, 'store']);
        Route::get('users/{id}/edit', [UserController::class, 'edit']);
        Route::post('users/{id}', [UserController::class, 'update']);
        Route::post('users/password/{id}', [UserController::class, 'updatePassword']);
        
    });

});






Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('view:cache');
    Artisan::call('config:cache');

    return "Cache is cleared";
}
);


require __DIR__.'/auth.php';
